public class Main {
    public static void main(String[] args) {
        NiceThread t1 = new NiceThread("NiceThread1");
        NiceThread t2 = new NiceThread("NiceThread2");
        NiceThread t3 = new NiceThread("NiceThread3");

        t1.start();
        t2.start();
        t3.start();
    }
}

class NiceThread extends Thread {
    String threadName;

    NiceThread(String threadName){
        this.threadName = threadName;
        this.setName(this.threadName);
    }

    public void run() {
        int i = 0;
        while (++i <= 9) {

            System.out.println(this.getName() + " - " + i);

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
